package com.anur.anursvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnursvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnursvcApplication.class, args);
	}

}
